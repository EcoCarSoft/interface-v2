/*
 * gsm.c
 *
 *  Created on: 27 wrz 2016
 *      Author: Pawel
 */

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#include <stdlib.h>
#include <string.h>

#include "data.h"
#include "gsm.h"
#include "gpio.h"
#include "usart.h"

extern xQueueHandle xGsmQueue;
extern xQueueHandle xGsmDataQueue;
extern xQueueHandle xDataQueue;

static char pingsend[16] = "AT+TCPSEND=0,5\r\n";


void vGsmTask(void *pvParameters){
	(void)pvParameters;

	DATA_STATUS xGsmDataStatus;
	DATA_STATUS *xDataPointer;
	GSM_STATUS xGsmStatus = GSM_INIT;

	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
	uint8_t xPingTick = 0;

	xGsmQueue = xQueueCreate(10, sizeof(char*));
	xGsmDataQueue = xQueueCreate(3, sizeof(DATA_STATUS*));

	hGsmStart();

	while(1){
		switch(xGsmStatus){
		case GSM_INIT:
			vTaskDelay(100);
			xGsmStatus = vGsmInit();
			break;
		case GSM_SETUP:
			vTaskDelay(100);
			xGsmStatus = vGsmSetup();
			break;
		case GSM_READY:
			hLedGsmSOff();
			vTaskDelay(100);
			if(!SW2){
				xGsmStatus = vGsmConnect();
				xLastWakeTime = xTaskGetTickCount();
			}
			break;
		case GSM_CONNECTED:
			hLedGsmSOn();
			if(SW2) xGsmStatus = vGsmDisconnect();
			else if(!SW3){
				xGsmStatus = GSM_SEND;
				xGsmDataStatus = GSM_START;
				DATA_STATUS *ptr = &xGsmDataStatus;
				xQueueSend(xDataQueue, &ptr, 0);
			}
			else if(xPingTick >= 10){
				xPingTick = 0;
				hLedGsmSOff();
				vGsmSendPing();
				hLedGsmSOn();
			}
			xPingTick++;
			vTaskDelayUntil(&xLastWakeTime, 100);
			break;
		case GSM_SEND:
			hLedGsmSOff();
			if(SW2){
				xGsmStatus = vGsmDisconnect();
				xGsmDataStatus = GSM_STOP;
				DATA_STATUS *ptr = &xGsmDataStatus;
				xQueueSend(xDataQueue, &ptr, 0);
				xLastWakeTime = xTaskGetTickCount();
			}
			else if(SW3){
				xGsmStatus = GSM_CONNECTED;
				xGsmDataStatus = GSM_STOP;
				DATA_STATUS *ptr = &xGsmDataStatus;
				xQueueSend(xDataQueue, &ptr, 0);
				xLastWakeTime = xTaskGetTickCount();
			}
			else if(xQueueReceive(xGsmDataQueue, &xDataPointer, 100) == pdTRUE){
				switch(*xDataPointer){
				case ENGINE_DATA:
					break;
				case GPS_DATA:
					hLedGsmSOn();
					vGsmSendGsmData(xDataPointer);
					hLedGsmSOff();
					break;
				}
			}
			break;
		case ERR_INIT:
			xGsmStatus = GSM_INIT;
			break;
		case ERR_CREG:
			xGsmStatus = GSM_SETUP;
			break;
		case ERR_TCPSETUP:
		case ERR_TCPCLOSE:
			hLedGsmSToggle();
			vTaskDelay(50);
			hLedGsmSToggle();
			vTaskDelay(50);
			hLedGsmSToggle();
			vTaskDelay(50);
			hLedGsmSToggle();
			xQueueReset(xGsmQueue);
			xGsmStatus = GSM_READY;
			break;
		default:
			hLedGsmSToggle();
			vTaskDelay(50);
			hLedGsmSToggle();
			vTaskDelay(50);
			hLedGsmSToggle();
			vTaskDelay(50);
			hLedGsmSToggle();
			vTaskDelay(500);
			xGsmStatus = GSM_INIT;
			break;
		}
	}
}


GSM_RESP vHandleResponse( char resp[] ){
	if(!strcmp("OK\r\n", resp)) return OK;
	else if(!strcmp("\r\nOK\r\n", resp)) return OK;
	else if(!strcmp("\r\n", resp)) return RN;
	else if(strchr(resp, '>')) return WR;
	else if(strstr(resp, "+TPCSEND:0")) return SEND_OK;
	else if(strstr(resp, "+TPCSEND:ERROR")) return SEND_ERR;
	else if(strstr(resp, "+TCPCLOSE:0,Link Closed")) return LINK_CLOSED;
	else if(strstr(resp, "+PBREADY")) return PBREADY;
	else if(strstr(resp, "MODEM:STARTUP")) return STARTUP;
	else if(strstr(resp, "+TCPSETUP:0,FAIL")) return SETUP_FAIL;
	else if(strstr(resp, "+TCPSETUP:0,OK")) return SETUP_OK;
	else if(strstr(resp, "+TCPCLOSE:0,OK")) return CLOSE_OK;
	return UNKNOWN;
}



GSM_STATUS vGsmInit(void){
	char *xGsmResponse;
	GSM_RESP r = UNKNOWN;

	xQueueReset(xGsmQueue);
	hGsmRstHigh();
	vTaskDelay( 100 );
	hGsmRstLow();

	xQueueReceive(xGsmQueue, &xGsmResponse, 10000);
	r = vHandleResponse(xGsmResponse);
	if(r != STARTUP) return ERR_INIT;

	vTaskDelay(10);

	hGsmSend("ATE0\r\n",6);
	xQueueReceive(xGsmQueue, &xGsmResponse, 200);
	xQueueReceive(xGsmQueue, &xGsmResponse, 200);
	r = vHandleResponse(xGsmResponse);

	vTaskDelay(10);
	xQueueReceive(xGsmQueue, &xGsmResponse, 60000);
	r = vHandleResponse(xGsmResponse);
	if(r != PBREADY) return ERR_INIT;

	return GSM_SETUP;
}

GSM_STATUS vGsmSetup(void){
	char *xGsmResponse;
	GSM_RESP r = UNKNOWN;
	uint8_t reg =0 , ret = 0, log = 0;

	xQueueReset(xGsmQueue);
	while(reg != '1'){
		hGsmSend("AT+CREG?\r\n",10);
		xQueueReceive(xGsmQueue, &xGsmResponse, 100);
		reg = xGsmResponse[11];
		if(ret == REG_RETRY) return ERR_CREG;
		else ret++;
		hLedGsmOn();
		vTaskDelay(50);
		hLedGsmOff();
		xQueueReceive(xGsmQueue, &xGsmResponse, 100);
		vTaskDelay(1000);
	}

	hGsmSend("AT+CSQ\r\n",9);
	while(xQueueReceive(xGsmQueue, &xGsmResponse, 100) == pdTRUE);
	r = vHandleResponse(xGsmResponse);

	hGsmSend("AT+XISP=0\r\n",11);
	while(xQueueReceive(xGsmQueue, &xGsmResponse, 1000) == pdTRUE);
	r = vHandleResponse(xGsmResponse);
	if(r != OK) return ERR_XISP;

	hGsmSend("AT+CGDCONT=1,\"IP\",\"internet\"\r\n",30);
	while(xQueueReceive(xGsmQueue, &xGsmResponse, 100) == pdTRUE);
	r = vHandleResponse(xGsmResponse);
	if(r != OK) return ERR_CGDCONT;

	hGsmSend("AT+XIIC=1\r\n",11);
	while(xQueueReceive(xGsmQueue, &xGsmResponse, 100) == pdTRUE);
	r = vHandleResponse(xGsmResponse);
	if(r != OK) return ERR_XIIC;

	ret = 0;
	while(log == pdFALSE){
		hGsmSend("AT+XIIC?\r\n",10);
		xQueueReceive(xGsmQueue, &xGsmResponse, 100);
		char *ip =  xGsmResponse+12;
		if(ip[0] == '1'){
			hLedGsmOn();
			log = pdTRUE;
		}
		else{
			hLedGsmOn();
			vTaskDelay(50);
			hLedGsmOff();
			vTaskDelay(50);
			hLedGsmOn();
			vTaskDelay(50);
			hLedGsmOff();
		}
		reg++;
		xQueueReceive(xGsmQueue, &xGsmResponse, 200);
		vTaskDelay(900);
		if(ret == REG_RETRY) return ERR_CREG;
	}

	return GSM_READY;
}

GSM_STATUS vGsmConnect(void){
	char *xGsmResponse;
	GSM_RESP r = UNKNOWN;
	GSM_STATUS ret = ERR_TCPSETUP;

	xQueueReset(xGsmQueue);
	hGsmSend("AT+TCPSETUP=0,91.227.122.43,1337\r\n",34);
	while(xQueueReceive(xGsmQueue, &xGsmResponse, 5000) == pdTRUE){
		r = vHandleResponse(xGsmResponse);
		if(r == SETUP_OK){
			ret = GSM_CONNECTED;
			break;
		}
	}

	return ret;
}

GSM_STATUS vGsmDisconnect(void){
	char *xGsmResponse;
	GSM_RESP r = UNKNOWN;
	GSM_STATUS ret = GSM_READY;

	xQueueReset(xGsmQueue);
	hGsmSend("AT+TCPCLOSE=0\r\n",15);
	while(xQueueReceive(xGsmQueue, &xGsmResponse, 5000) == pdTRUE){
		r = vHandleResponse(xGsmResponse);
		if(r == CLOSE_OK){
			ret = GSM_READY;
			break;
		}
	}

	return ret;
}

GSM_STATUS vGsmHandleConnection(void){
	char *xGsmResponse;
	GSM_RESP r = UNKNOWN;
	GSM_STATUS ret = GSM_CONNECTED;

	while(xQueueReceive(xGsmQueue, &xGsmResponse, 5) == pdTRUE){
		r = vHandleResponse(xGsmResponse);
		if(r == SEND_ERR){

		}
		if(r == LINK_CLOSED){
			ret = vGsmDisconnect();
		}
	}
	return ret;
}

void vGsmSendPing(void){
	char *xGsmResponse;
	GSM_RESP r = UNKNOWN;

	xQueueReset(xGsmQueue);
	hGsmSend(pingsend,16);
	while(xQueueReceive(xGsmQueue, &xGsmResponse, 300) == pdTRUE){
		r = vHandleResponse(xGsmResponse);
		if(r == WR){
			vTaskDelay(10);
			hGsmSend("PING!\r",6);
			break;
		}
	}
}

void vGsmSendGsmData(DATA_STATUS *gpsData){
	char *xGsmResponse;
	GSM_RESP r = UNKNOWN;

	xQueueReset(xGsmQueue);
	hGsmSend("AT+TCPSEND=0,22\r\n",17);
	while(xQueueReceive(xGsmQueue, &xGsmResponse, 300) == pdTRUE){
		r = vHandleResponse(xGsmResponse);
		if(r == WR){
			vTaskDelay(10);
			hGsmSend( (char*) gpsData,23);
			break;
		}
	}
}
