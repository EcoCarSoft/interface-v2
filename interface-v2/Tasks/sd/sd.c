/*
 * sd.c
 *
 *  Created on: 10 cze 2016
 *      Author: Pawel
 */

#include <stdlib.h>
#include <string.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#include "ff.h"
#include "sd.h"
#include "gpio.h"

#include "rtc.h"
#include "data.h"

extern xQueueHandle xSdDataQueue;
extern xQueueHandle xDataQueue;

void vSdTask(void *pvParameters){
	(void)pvParameters;

	char xSdBuff[256];
	SD_STATUS xSdStatus = SD_NOT_MOUNT;
	DATA_STATUS xSdDataStatus;
	DATA_STATUS *xDataPointer;
	xSdDataQueue = xQueueCreate(2, sizeof(DATA_STATUS*));

	while(1){
		switch(xSdStatus){
		case SD_NOT_MOUNT:
			if(!SD_CD){
				hLedSDOn();
				xSdStatus = mount_card();
			}
			vTaskDelay(100);
			break;
		case SD_MOUNT:
			if(!SW1){
				xSdStatus = open_files(xSdBuff);
				hLedSDOff();
				xSdDataStatus = SD_START;
				DATA_STATUS *ptr = &xSdDataStatus;
				xQueueSend(xDataQueue, &ptr, 0);
			}
			if(SD_CD){
				hLedSDOff();
				xSdStatus = SD_NOT_MOUNT;
			}
			vTaskDelay(100);
			break;
		case SD_WRITE:
			if(xQueueReceive(xSdDataQueue, &xDataPointer, 100) == pdTRUE){
				switch(*xDataPointer){
				case ENGINE_DATA:
					break;
				case GPS_DATA:
					hLedSDOn();
					xSdStatus = write_gps_data(xSdBuff, (GPS_Struct*)xDataPointer);
					vTaskDelay(25);
					hLedSDOff();
					break;
				}
			}
			if(SW1 | SD_CD){
				hLedSDOn();
				xSdStatus = close_files();
				xSdDataStatus = SD_STOP;
				DATA_STATUS *ptr = &xSdDataStatus;
				xQueueSend(xDataQueue, &ptr, 0);
			}
			break;
		case SD_FAULT:
			vTaskDelay(250);
			hLedSDToggle();
			if(SD_CD){
				hLedSDOff();
				xSdStatus = SD_NOT_MOUNT;
			}
			break;
		}
	}
}


SD_STATUS mount_card(void){
	FRESULT res = 0;
	res = f_mount(&FatFs, "", 1);
	if (res == FR_OK) return SD_MOUNT;
	return SD_FAULT;
}


SD_STATUS open_files(char *sdBuff){
	FRESULT res_f1 = 0;
	FRESULT res_f2 = 0;
	int len_f1 = 0;
	int len_f2 = 0;

	res_f1 = f_open(&fil1, "gps.txt", FA_OPEN_ALWAYS | FA_READ | FA_WRITE);
	if (res_f1 == FR_OK) {
		char tmp[4];
		res_f1 = f_lseek(&fil1, f_size(&fil1));
		sprintf(sdBuff, "\r\n\r\nEcoCar training session GPS data. Start day: ");
		uint16_t date = xRtcGetDay();
		ud2a( date ,tmp);
		strncat(sdBuff, tmp, 4);
		strncat(sdBuff, ".", 1);
		date = xRtcGetMonth();
		ud2an(date, tmp, 2);
		strncat(sdBuff, tmp, 4);
		strncat(sdBuff, ".", 1);
		date = xRtcGetYear();
		ud2a( date,tmp );
		strncat(sdBuff, tmp, 4);
		strncat(sdBuff, "\r\nHour[h:m:s]\tLat[ddmm.mmm]\tLong[ddmm.mmm]\tSpd[K]\tCrs[*]\tFix[0,1]\r\n", 67);
		len_f1 = f_puts(sdBuff, &fil1);
	}
	res_f2 = f_open(&fil2, "data.txt", FA_OPEN_ALWAYS | FA_READ | FA_WRITE);
	if (res_f2 == FR_OK) {
		res_f2 = f_lseek(&fil2, f_size(&fil2));
		sprintf(sdBuff, "Test karty sd plik data.\r\n");
		len_f2 = f_puts(sdBuff, &fil2);
	}
	if((len_f1 > 0) && (len_f2 > 0)) return SD_WRITE;
	return SD_FAULT;
}

SD_STATUS close_files(void){
	FRESULT res2 = 0;
	FRESULT res3 = 0;

	res2 = f_close(&fil1);
	res3 = f_close(&fil2);
	if ((res2 == FR_OK) && (res3 == FR_OK)) return SD_MOUNT;
	return SD_FAULT;
}


SD_STATUS write_gps_data(char *sdBuff,GPS_Struct *gps_data){
	int len_f1 = 0;
	char tmp[5];
	char tmpch[2] = " \0";
	//uint32_t msec = hRtcGetTime();
	msec2hms(gps_data->time, sdBuff);
	strncat(sdBuff, "\t", 1);

	int32_t dec = gps_data->latitude / 100000;
	uint32_t frac = gps_data->latitude % 100000;
	frac &= ~(1<<31);
	d2a(dec, tmp);
	strncat(sdBuff, tmp, 5);
	strncat(sdBuff, ".", 1);
	ud2an(frac, tmp, 5);
	strncat(sdBuff, tmp, 5);
	strncat(sdBuff, "\t", 1);

	dec = gps_data->longitude / 100000;
	frac = gps_data->longitude % 100000;
	frac &= ~(1<<31);
	d2a(dec, tmp);
	strncat(sdBuff, tmp, 5);
	strncat(sdBuff, ".", 1);
	ud2an(frac, tmp, 5);
	strncat(sdBuff, tmp, 5);
	strncat(sdBuff, "\t", 1);

	dec = gps_data->speed / 10;
	frac = gps_data->speed % 10;
	d2a(dec, tmp);
	strncat(sdBuff, tmp, 3);
	strncat(sdBuff, ".", 1);
	tmpch[0] = frac + '0';
	strncat(sdBuff, tmpch, 1);
	strncat(sdBuff, "\t", 1);

	dec = gps_data->course / 10;
	frac = gps_data->course % 10;
	d2a(dec, tmp);
	strncat(sdBuff, tmp, 3);
	strncat(sdBuff, ".", 1);
	tmpch[0] = frac + '0';
	strncat(sdBuff, tmpch, 1);
	strncat(sdBuff, "\t", 1);

	tmpch[0] = gps_data->fix + '0';
	strncat(sdBuff, tmpch, 2);
	strncat(sdBuff, "\r\n", 2);

	len_f1 = f_puts(sdBuff, &fil1);
	if(len_f1 > 0) return SD_WRITE;
	return SD_FAULT;
}


char* ud2a(int value, char* buffer){
	static const char digits[] = "0123456789";
	char* buffer_copy = buffer;
	char temp;
	int32_t quot, rem;

	do{
		quot = value / 10;
		rem = value % 10;
		*buffer++ = digits[rem];
	} while ((value = quot));

	*buffer = '\0';
	buffer--;

	while (buffer > buffer_copy)
	{
		temp = *buffer;
		*buffer-- = *buffer_copy;
		*buffer_copy++ = temp;
	}
	return buffer_copy;
}

char* ud2an(int value, char* buffer, uint8_t n){
	static const char digits[] = "0123456789";
	char* buffer_copy = buffer;
	char temp;
	int32_t rem;

	while(n){
		rem = value % 10;
		*buffer++ = digits[rem];
		value /= 10;
		n--;
	};
	*buffer = '\0';
	buffer--;

	while (buffer > buffer_copy)
	{
		temp = *buffer;
		*buffer-- = *buffer_copy;
		*buffer_copy++ = temp;
	}
	return buffer_copy;
}

char* d2a(int value, char* buffer){
	static const char digits[] = "0123456789";
	char* buffer_copy = buffer;
	char temp;
	int32_t quot, rem;
	uint8_t neg = 0;

	if(value < 0) {
		neg = 1;
		value *= -1;
	}
	do {
		quot=value / 10;
		rem=value % 10;
		*buffer++ = digits[rem];
	} while ((value = quot));

	if(neg) *buffer++ = '-';
	*buffer = '\0';
	buffer--;

	while (buffer > buffer_copy)
	{
		temp = *buffer;
		*buffer-- = *buffer_copy;
		*buffer_copy++ = temp;
	}
	return buffer_copy;
}

char* msec2hms(uint32_t sec, char* buffer){
	uint8_t h = sec / 36000;
	uint8_t m = sec / 600 - h * 60;
	uint8_t s = ( sec % 600 ) / 10;
	uint8_t ms = sec % 10;

	*buffer++ = h / 10 + '0';
	*buffer++ = h % 10 + '0';
	*buffer++ = ':';
	*buffer++ = m / 10 + '0';
	*buffer++ = m % 10 + '0';
	*buffer++ = ':';
	*buffer++ = s / 10 + '0';
	*buffer++ = s % 10 + '0';
	*buffer++ = '.';
	*buffer++ = ms + '0';
	*buffer = '\0';
	return buffer;
}

DWORD get_fattime(void){
	uint32_t t = hRtcGetTime();
	uint8_t h = t / 36000;
	uint8_t m = (t % 36000) / 600;
	uint8_t s = (t % 600) / 10;
	uint8_t d = xRtcGetDay();

	return (((xRtcGetYear() - 1980) << 25) 	// Year 2014
			| (xRtcGetMonth()  << 21) 			// Month 7
			| (d << 16) 			// Mday 10
			| (h << 11)			// Hour 16
			| (m << 5) 				// Min 0
			| (s >> 1)); 			// Sec 0
}

